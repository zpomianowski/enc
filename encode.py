# -*- coding: utf-8 -*-
## to encode
IN_FILE = u'input/obrona_1.mp4'
OUT_FILE = u'output/obrona_1'

import os
import codecs
import random

class Transcoder:
    ## video Transcoder
    __venc = {'x264':{
            'path':'x264.exe', 'suffix':'.mp4',
            'presets':{
                '1pass':{
                    'flow':['--bitrate 1500 --profile high --output "&VOUT" "&VIN"'],
                    'filters':[
                        dict(sO=0,  sE=1, type='video', sF='deinterlace', sM='yadif',    mode=0, order=-1),
                        dict(sO=1,  sE=1, type='video', sF='resize',      sM='bicubic',  width=640, height=360, b=0, c=0.6),
                    ],
                    'avs':True,
                },
                '2pass':{
                    'flow':['--pass 1 --bitrate 1500  --profile high --output NUL "&VIN"', '--pass 2 --bitrate 1500 --profile high --output "&VOUT" "&VIN"'],
                    'filters':[],
                    'avs':True,
                },
                'lossless':{
                    'flow':['--qp 0 --output "&VOUT" "&VIN"'],
                    'filters':[],
                    'avs':True,
                },
                'HD1080p':{
                    'flow':['--pass 1 --bitrate 3500 --ratetol 40 --stats "&STATS" --ref 16 --open-gop --bframes 16 --b-adapt 2 --no-fast-pskip --direct auto --deblock -1:-1 --subme 10 --partitions all --me umh --merange 32 --output NUL "&VIN"',
                            '--pass 2 --bitrate 3500 --ratetol 40 --stats "&STATS" --ref 16 --open-gop --bframes 16 --b-adapt 2 --no-fast-pskip --direct auto --deblock -1:-1 --subme 10 --partitions all --me umh --merange 32 --output "&VOUT" "&VIN"',],
                    'filters':[
                        dict(sO=1, sE=1, type='video', sF='resize', sM='bicubic', width=1920, height=1080, b=0, c=0.6),
                    ],
                    'avs':True,
                },
                'HD720p':{
                    'flow':['--pass 1 --bitrate 2300 --ratetol 35 --stats "&STATS" --ref 16 --open-gop --bframes 16 --b-adapt 2 --no-fast-pskip --direct auto --deblock -1:-1 --subme 10 --partitions all --me umh --merange 32 --output NUL "&VIN"',
                            '--pass 2 --bitrate 2300 --ratetol 35 --stats "&STATS" --ref 16 --open-gop --bframes 16 --b-adapt 2 --no-fast-pskip --direct auto --deblock -1:-1 --subme 10 --partitions all --me umh --merange 32 --output "&VOUT" "&VIN"',],
                    'filters':[
                        dict(sO=1, sE=1, type='video', sF='resize', sM='bicubic',  width=1280, height=720, b=0, c=0.6),
                    ],
                    'avs':True,
                },
                'SD576p':{
                    'flow':['--pass 1 --bitrate 1570 --ratetol 30 --stats "&STATS" --ref 16 --open-gop --bframes 16 --b-adapt 2 --no-fast-pskip --direct auto --deblock -1:-1 --subme 10 --partitions all --me umh --merange 32 --output NUL "&VIN"',
                            '--pass 2 --bitrate 1570 --ratetol 30 --stats "&STATS" --ref 16 --open-gop --bframes 16 --b-adapt 2 --no-fast-pskip --direct auto --deblock -1:-1 --subme 10 --partitions all --me umh --merange 32 --output "&VOUT" "&VIN"',],
                    'filters':[
                        dict(sO=1, sE=1, type='video', sF='resize', sM='bicubic',  width=1024, height=576, b=0, c=0.6),
                    ],
                    'avs':True,
                },
                'MQ336p':{
                    'flow':['--pass 1 --bitrate 552 --ratetol 20 --stats "&STATS" --ref 16 --open-gop --bframes 16 --b-adapt 2 --no-fast-pskip --direct auto --deblock -1:-1 --subme 10 --partitions all --me umh --merange 16 --output NUL "&VIN"',
                            '--pass 2 --bitrate 552 --ratetol 20 --stats "&STATS" --ref 16 --open-gop --bframes 16 --b-adapt 2 --no-fast-pskip --direct auto --deblock -1:-1 --subme 10 --partitions all --me umh --merange 16 --output "&VOUT" "&VIN"',],
                    'filters':[
                        dict(sO=1, sE=1, type='video', sF='resize', sM='bicubic',  width=592, height=336, b=0, c=0.6),
                    ],
                    'avs':True,
                },
                'prev':{
                    'flow':['--bitrate 2300 --ratetol 50 --preset fast --output "&VOUT" "&VIN"'],
                    'filters':[
                        dict(sO=1, sE=1, type='video', sF='resize', sM='bicubic',  width=592, height=336, b=0, c=0.6),
                    ],
                    'avs':True,
                },
                'rj-y':{
                    'flow':['--crf 23 --output "&VOUT" "&VIN"'],
                    'filters':[                    
                        dict(sO=0,  sE=1, type='video', sF='deinterlace', sM='yadif', mode=0, order=-1),
                    ],
                    'avs':True,
                },
                'rj-tff':{
                    'flow':['--crf 23 --tff --output "&VOUT" "&VIN"'],
                    'filters':[],
                    'avs':True,
                },
                'rj-bff':{
                    'flow':['--crf 23 --bff --output "&VOUT" "&VIN"'],
                    'filters':[],
                    'avs':True,
                },
            },
        },
    }
    
    ## audio Transcoder
    __aenc = {'nero':{
            'path':'neroAacEnc.exe', 'suffix':'.m4a',
            'presets':{
                '128lc':{
                    'flow':['-lc -cbr 128000 -if - -of "&AOUT"'],
                    'filters':[],
                    'avs':True    
                },
                '320lc':{
                    'flow':['-lc -cbr 320000 -if - -of "&AOUT"'],
                    'filters':[],
                    'avs':True    
                },
            },
        },
    }

    ## muxer
    __muxer = {'mp4box':{
             'path':'mp4box.exe', 'suffix':'.mp4',
             'flow':[' -add "&VTEMP" -add "&ATEMP" -new "&VOUT"']
             },
    }

    __bepipe = {'wavi':{
                'path':'wavi.exe',
                'flow':['&AIN - |']
                }        
    }

    __cleanjobs = []        

    def __init__(self, vi=None, vo=None, ai=None, ao=None,
                 venc='x264', aenc='nero', vpreset='rj-tff', apreset='128lc',
                 mux='mp4box', apipe='wavi',workflow='audiovideomux', forder='tff', fps=25):       
        self.__VIN = vi
        self.__VOUT = vo
        self.__AIN = ai
        self.__AOUT = ao
        self.__vencIndex = venc
        self.__aencIndex = aenc
        self.__muxIndex = mux
        self.__bepipeIndex = apipe
        self.__vpreset = self.__venc[venc]['presets'][vpreset]
        self.__apreset = self.__aenc[aenc]['presets'][apreset]
        self.__mpreset = self.__muxer[mux]
        self.__bepipepreset = self.__bepipe[apipe]


        self.__avsv = '%d.avs' % (random.random() * 10**50)
        self.__avsa = '%d.avs' % (random.random() * 10**50)
        self.__bat  = '%d.bat' % (random.random() * 10**50)
        self.__stats = '%d' % (random.random() * 10**50)
        self.__cleanjobs.append(self.__avsv)
        self.__cleanjobs.append(self.__avsa)
        self.__cleanjobs.append(self.__bat)
        ## chose only one
        self.__QT_SOURCE = False
        self.__DS_SOURCE = False
        self.__FF_SOURCE = True
        self.__AVC_SOURCE = False   
        self.__FPS = fps
        self.__FIELDORDER = forder
        # video, audio, videoaudio, videoaudiomux 
        self.__workflow = workflow
        
        ## additional sfilter, examples    
        # sO - order, type - use for video/audio, sE - enabled/disabled, sF - function, sM - method
        self.__advanced = [
            dict(sO=0,  sE=0, type='video', sF='deinterlace', sM='yadif',    mode=0, order=-1),
            dict(sO=2,  sE=0, type='video', sF='resize',      sM='bicubic',  width=640, height=360, b=0, c=0.6),
            dict(sO=3,  sE=0, type='video', sF='resize',      sM='bilinear', width=320, height=180),
            dict(sO=4,  sE=0, type='video', sF='resize',      sM='lanczos',  width=1280, height=720, taps=4),
            dict(sO=5,  sE=0, type='video', sF='resize',      sM='point',    width=640, height=576),
            dict(sO=5,  sE=0, type='video', sF='resize',      sM='spline16', width=1280, height=720),
            dict(sO=6,  sE=0, type='video', sF='resize',      sM='spline36', width=1280, height=720),
            dict(sO=7,  sE=0, type='video', sF='resize',      sM='gauss',    width=1280, height=720, p=30),
            dict(sO=8,  sE=0, type='video', sF='crop',        sM='',         l=10, r=-10, t=10, b=-10),
            dict(sO=9,  sE=0, type='video', sF='trim',        sM='',         start=0, stop=100),
            dict(sO=10, sE=0, type='video', sF='script',      sM='',         path='scripts/slowmo.avs', desc='Slow motion', func='', args=[600, self.__FPS]),
            dict(sO=10, sE=0, type='video', sF='script',      sM='',         path='scripts/msu_frc.avs', desc='Slow motion MSU_FRC', func='', args=[20, '"slow"', self.__FPS]),
            dict(sO=10, sE=0, type='video', sF='script',      sM='',         path='scripts/msu_denoiser.avs', desc='Denoiser', func='', args=[0, 0, 0, 1]),
            dict(sO=10, sE=0, type='video', sF='script',      sM='',         path='scripts/msu_oldfilm.avs', desc='Old style', func='', args=[]),
            dict(sO=10, sE=0, type='video', sF='ffdshow',     sM='',         preset='"denoise"', args=['isLevels=1','levelsMode=0','levelsGamma=1500']),
            dict(sO=10, sE=0, type='video', sF='script',      sM='',         path='scripts/msu_cartonizer.avs', desc='Cartoon', func='', args=[0, # 0-paint,1-fill,2-draw
                                                                                                                                               1, # ShowEdges[0,1]
                                                                                                                                               1, # EdgeAmount[0,10]
                                                                                                                                               5, # EdgeBrightness[0,10]
                                                                                                                                               1, # EdgeThickness[1,4] 
                                                                                                                                               4, # EdgeAccuracy[1,4]
                                                                                                                                               1, # UseFaceDetection[0,1]
                                                                                                                                               1, # BrushSize[1,10]
                                                                                                                                               5, # BrushWetness[0,10]
                                                                                                                                               0, # ColorRandom[0,10]
                                                                                                                                               5, # ColorSaturation[0,10]
                                                                                                                                               1  # ProcessEachNframe[1,10]
                                                                                                                                               ]),
            ]
       
    def setup(self):        
        for f in self.__vpreset['filters']:
            self.__advanced.append(f)
        for f in self.__apreset['filters']:
            self.__advanced.append(f)
        self.__makeAvsv()
        self.__makeAvsa()
        self.__makeBat()

    """ qt - QTInput, ds - DirectShow, ff - FFMPEG """
    def setSource(self, option):
        if option == 'qt':        
            self.__QT_SOURCE = True
            self.__DS_SOURCE = False
            self.__FF_SOURCE = False
            self.__AVC_SOURCE = False
        if option == 'ds':        
            self.__QT_SOURCE = False
            self.__DS_SOURCE = True
            self.__FF_SOURCE = False
            self.__AVC_SOURCE = False
        if option == 'ff':        
            self.__QT_SOURCE = False
            self.__DS_SOURCE = False
            self.__FF_SOURCE = True
            self.__AVC_SOURCE = False
        if option == 'avc':        
            self.__QT_SOURCE = False
            self.__DS_SOURCE = False
            self.__FF_SOURCE = False
            self.__AVC_SOURCE = True
    
    def getVideoPreset(self):
        return self.__vpreset

    def getAudioPreset(self):
        return self.__apreset

    def getMuxerPreset(self):
        return self.__mpreset

    def __appendFilters(self, pType):
        ## additional filtering
        lScript = ''
        #lScript += 'AudioDub(video, audio)\n'
        filters = sorted([item for item in self.__advanced if pType in item['type'] and item['sE'] == True], key=lambda k: k['sO'])
        for filter in filters:
            if filter['sF'] == 'deinterlace':
                if filter['sM'] == 'yadif':
                    lScript += 'Weave()\n'
                    lScript += 'loadCPlugin("avsplugins\yadif.dll")\n'
                    lScript += 'Yadif(%s, %s)\n' % (filter['mode'], filter['order'])
            if filter['sF'] == 'resize':
                if filter['sM'] == 'bilinear':
                    lScript += 'BilinearResize(%s, %s)\n' % (filter['width'], filter['height'])
                if filter['sM'] == 'bicubic':
                    lScript += 'BicubicResize(%s, %s, %s, %s)\n' % (filter['width'], filter['height'], filter['b'], filter['c'])
                if filter['sM'] == 'lanczos':
                    lScript += 'LanczosResize(%s, %s, %s)\n' % (filter['width'], filter['height'], filter['taps'])
                if filter['sM'] == 'point':
                    lScript += 'PointResize(%s, %s)\n' % (filter['width'], filter['height'])
                if filter['sM'] == 'spline16':
                    lScript += 'Spline16Resize(%s, %s)\n' % (filter['width'], filter['height'])
                if filter['sM'] == 'spline36':
                    lScript += 'Spline36Resize(%s, %s)\n' % (filter['width'], filter['height'])
                if filter['sM'] == 'gauss':
                    lScript += 'GaussResize(%s, %s, %s)\n' % (filter['width'], filter['height'], filter['p'])
            if filter['sF'] == 'crop':                
                lScript += 'Crop(%s, %s, %s, %s)\n' % (filter['l'], filter['t'], filter['r'], filter['b'])
            if filter['sF'] == 'trim':
                if 'audio' in self.__workflow:
                    lScript += 'AudioTrim(%s, %s)\n' % (filter['start']/self.__FPS, filter['stop']/self.__FPS)
                if 'video' == self.__workflow:
                    lScript += 'Trim(%s, %s)\n' % (filter['start'], filter['stop'])
            if filter['sF'] == 'script':
                f = open(filter['path'])
                content = f.read()
                f.close()
                _script = u'#%s\n%s\n' % (filter['desc'], content)
                if filter['func'] != '':
                    _script += '%s(' % (filter['func'])
                    for i in range(len(filter['args'])):
                        _script += '%s' % (filter['args'][i])
                        if i != len(filter['args']) - 1: _script += ','
                    _script += ')\n'
                else:
                    if len(filter['args']) > 0: _script = _script % tuple(filter['args'])
                lScript += _script
            if 'ffdshow' in filter['sF']:
                lScript += '%s(%s,' % (filter['sF'], filter['preset'])
                for i in range(len(filter['args'])):
                    lScript += '%s' % (filter['args'][i])
                    if i != len(filter['args']) - 1: lScript += ','
                lScript += ')\n'
        return lScript

    def __makeAvsv(self):
        script = ''
        if self.__QT_SOURCE:
            script += 'LoadPlugin("avsplugins\QTSource.dll")\n'
            script += u'QTInput("%s")\n' % (self.__VIN)
        if self.__DS_SOURCE:
            script += u'DirectShowSource("%s")\n' % (self.__VIN)
        if self.__FF_SOURCE:
            script += 'LoadCPlugin("avsplugins\\ffms2.dll")\n'
            script += 'Import("avsplugins\FFMS2.avsi")\n' 
            script += u'FFIndex("%s")\n' % (self.__VIN)
            script += u'FFVideoSource("%s")\n' % (self.__VIN)
            self.__cleanjobs.append(self.__VIN + '.ffindex')            
        if self.__AVC_SOURCE:
            script += 'LoadPlugin("avsplugins\dgavc\DGAVCDecode.dll")\n'
            script += 'AVCSource("test.dga",deblock=false)\n'
        #script += 'ConvertToYUV2()\n'
        if self.__FIELDORDER == 'tff': script += 'AssumeTFF()\n'
        if self.__FIELDORDER == 'bff': script += 'AssumeBFF()\n'
        if self.__FIELDORDER != 'prog':
            script += 'SeparateFields()\n'
        script += self.__appendFilters('video')
        
        f = codecs.open(self.__avsv, 'w', 'utf-8')
        f.write(script)
        f.close()

    def __makeAvsa(self):
        script = ''
        if self.__QT_SOURCE:
            script += 'LoadPlugin("avsplugins\QTSource.dll")\n'
            script += u'QTInput("%s")\n' % (self.__VIN)
        if self.__DS_SOURCE:
            script += u'DirectShowSource("%s")\n' % (self.__VIN)
        else:
            script += 'LoadCPlugin("avsplugins\\ffms2.dll")\n'
            script += 'Import("avsplugins\FFMS2.avsi")\n' 
            script += u'FFIndex("%s")\n' % (self.__VIN)
            script += u'FFAudioSource("%s")\n' % (self.__VIN)        
            script += self.__appendFilters('audio')

        f = codecs.open(self.__avsa, 'w', 'utf-8')
        f.write(script)
        f.close()

    def __makeBat(self):
        vscript = ''
        ascript = ''
        mscript = ''
        if 'video' in self.__workflow:
            if len(self.__vpreset['flow']) > 1:
                self.__cleanjobs.append(self.__stats)
                self.__cleanjobs.append(self.__stats + '.mbtree')
            for s in self.__vpreset['flow']:
                vscript += '%s %s\n' % (self.__venc[self.__vencIndex]['path'], s)
                vscript = self.__parse(vscript, self.__vpreset['avs'])
        
        if 'audio' in self.__workflow:
            for s in self.__apreset['flow']:
                if self.__apreset['avs']:
                    for f in self.__bepipepreset['flow']:
                        ascript += '%s %s ' % (self.__bepipe[self.__bepipeIndex]['path'], f)
                ascript += '%s %s\n' % (self.__aenc[self.__aencIndex]['path'], s)
                ascript = self.__parse(ascript, self.__apreset['avs'])
        
        if 'mux' in self.__workflow:
            for s in self.__mpreset['flow']:
                mscript += '%s %s\n' % (self.__muxer[self.__muxIndex]['path'], s)
                mscript = self.__parse(mscript, -1)                
                self.__cleanjobs.append(self.__VOUT + '_video' + self.__venc[self.__vencIndex]['suffix'])
                self.__cleanjobs.append(self.__AOUT + '_audio' + self.__aenc[self.__aencIndex]['suffix'])

        f = codecs.open(self.__bat, 'w', 'utf-8')
        f.write(vscript + ascript + mscript)
        f.close()

    def __parse(self, s, flag):
        vsuf = self.__venc[self.__vencIndex]['suffix']
        asuf = self.__aenc[self.__aencIndex]['suffix']
        msuf = self.__muxer[self.__muxIndex]['suffix']
        self.__AOUT = [self.__AOUT, self.__VOUT][self.__AOUT == None]
        self.__AIN = [self.__AIN, self.__VIN][self.__AIN == None]
        if flag:
            s = s.replace('&VIN', self.__avsv)
            s = s.replace('&AIN', self.__avsa)
        else:
            s = s.replace('&VIN', self.__VIN)
            s = s.replace('&AIN', self.__AIN)
        if flag != -1:
            s = s.replace('&VOUT', self.__VOUT + '_video' + vsuf)
            s = s.replace('&AOUT', self.__AOUT + '_audio' + asuf)
        else:
            s = s.replace('&VOUT', self.__VOUT + msuf)
            s = s.replace('&VTEMP', self.__VOUT + '_video' + vsuf)
            s = s.replace('&ATEMP', self.__AOUT + '_audio' + asuf)
        s = s.replace('&STATS', self.__stats)
        return s

    def __execute(self):
        os.system(self.__bat)

    def __clear(self):
        for j in self.__cleanjobs:
            try:
                os.remove(j)
            except Exception as e:
                print e

    def run(self):
        self.__execute()
        self.__clear()
        pass

if __name__ == '__main__':
    e = Transcoder(IN_FILE, OUT_FILE, vpreset='HD1080p', apreset='128lc', forder='prog', workflow='video')
    e.setSource('ff')
    e.setup()
    e.run()

