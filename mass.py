from encode import Transcoder
import os
import re

FROM = 'input'
TO   = 'output'
SUFFIX = 'mts'

if __name__ == '__main__':
    files = ['%s\%s' % (FROM, f) for f in os.listdir('input') if re.match(r'.*\.%s' % (SUFFIX), f, re.IGNORECASE)]
    print files
    for file in files:
        e = Transcoder(file, '%s\%s' % (TO, os.path.basename(file)), vpreset='rf-tff', apreset='320lc', forder='tff')
        e.setSource('ds')
        e.setup()
        e.run()

